﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CubeGame {
    class Cube : DrawableGameComponent {
        SmallCube[] cubes;

        List<SmallCube>[] YWalls;
        List<SmallCube>[] XWalls;
        List<SmallCube>[] ZWalls;

        List<SmallCube>[] visibleWalls;

        int cubesCount = (int)Math.Pow(GameConfig.sideSize, 3);

        float rotateAmountX;
        float rotateAmountY;

        Vector2 mouseOld;
        ContentManager content;

        Matrix view;
        Matrix projection;

        Texture2D[] colors;

        //animation
        bool wallIsRotating;
        float currentRotationA;
        List<SmallCube> rotatingWall;
        Vector3 rotationAxis;


        public Cube(Game game, ContentManager content) : base(game){
            cubes = new SmallCube[cubesCount];
            mouseOld = new Vector2(0f, 0f);
            rotateAmountX = 0f;
            rotateAmountY = 0f;
            this.content = content;
            XWalls = new List<SmallCube>[GameConfig.sideSize];
            YWalls = new List<SmallCube>[GameConfig.sideSize];
            ZWalls = new List<SmallCube>[GameConfig.sideSize];
            for (int i = 0; i < GameConfig.sideSize; i++) {
                XWalls[i] = new List<SmallCube>();
                YWalls[i] = new List<SmallCube>();
                ZWalls[i] = new List<SmallCube>();
            }

            visibleWalls = new List<SmallCube>[6];

            visibleWalls[0] = XWalls[GameConfig.sideSize - 1];
            visibleWalls[1] = YWalls[GameConfig.sideSize - 1];
            visibleWalls[2] = ZWalls[GameConfig.sideSize - 1];
            visibleWalls[3] = XWalls[0];
            visibleWalls[4] = YWalls[0];
            visibleWalls[5] = ZWalls[0];

            colors = new Texture2D[6];

            LoadContent();

            SetWallColors();
        }

        public bool isSolved(){
            foreach (List<SmallCube> wall in visibleWalls) {
                bool[] colorPosition = { true, true, true, true, true, true };
                SmallCube firstCube = wall[0];
                for (int cube = 1; cube < GameConfig.sideSize * GameConfig.sideSize; cube++) {
                    bool solvedPossible = false;
                    for (int i = 0; i < 6; i++) {
                        if (true == colorPosition[i]) {
                            bool flag = (firstCube.getWallTexture((SmallCube.walls)i) != null) &&
                                (Texture2D.ReferenceEquals(firstCube.getWallTexture((SmallCube.walls)i), wall[cube].getWallTexture((SmallCube.walls)i)));
                            solvedPossible = solvedPossible || flag;
                            if (!flag) colorPosition[i] = false;
                        }
                    }
                    if (false == solvedPossible) return false;
                }
            }
            return true;
        }

        protected override void LoadContent() {
            view = Matrix.CreateLookAt(new Vector3(0f, 0f, 300f), new Vector3(1f, 1f, 1f), Vector3.Up);
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45f),Game.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000.0f);

            Texture2D baseTexture = content.Load<Texture2D>("Content/Textures/metal-texture");

            colors[0] = content.Load<Texture2D>("Content/Textures/Colors/green");
            colors[1] = content.Load<Texture2D>("Content/Textures/Colors/blue");
            colors[2] = content.Load<Texture2D>("Content/Textures/Colors/red");
            colors[3] = content.Load<Texture2D>("Content/Textures/Colors/yellow");
            colors[4] = content.Load<Texture2D>("Content/Textures/Colors/white");
            colors[5] = content.Load<Texture2D>("Content/Textures/Colors/orange");

            int count = 0;
            for (int z = 0; z < GameConfig.sideSize; z++) {
                for (int x = 0; x < GameConfig.sideSize; x++) {
                    for (int y = 0; y < GameConfig.sideSize; y++) {
                        SmallCube smallCube = cubes[count++] = new SmallCube(
                                            new Vector3((x - 1) * 31, (y - 1) * 31, (z - 1) * 31),
                                            new Vector3(1f, 1f, 1f),
                                            content, baseTexture);
                        XWalls[x].Add(smallCube);
                        YWalls[y].Add(smallCube);
                        ZWalls[z].Add(smallCube);
                    }
                }
            }
            base.LoadContent();
        }

        public override void Update(GameTime gameTime) {
            float eTime = gameTime.ElapsedGameTime.Milliseconds;

            //rotate the cube
            if (Mouse.GetState().RightButton == ButtonState.Pressed) {
                rotateAmountX += 0.0005f * eTime * (Mouse.GetState().X - mouseOld.X);
                rotateAmountY += 0.0005f * eTime * (Mouse.GetState().Y - mouseOld.Y);
                Vector3 rotation = new Vector3(rotateAmountY, rotateAmountX, 0);
                RotateCubes(rotation);
            }

            if (false == wallIsRotating) {
                if (Keyboard.GetState().IsKeyDown(Keys.Z)) {
                    wallIsRotating = true;
                    rotationAxis = Vector3.UnitX;
                    rotatingWall = XWalls[0];
                } else if (Keyboard.GetState().IsKeyDown(Keys.X)) {
                    wallIsRotating = true;
                    rotationAxis = Vector3.UnitX;
                    rotatingWall = XWalls[1];
                } else if (Keyboard.GetState().IsKeyDown(Keys.C)) {
                    wallIsRotating = true;
                    rotationAxis = Vector3.UnitX;
                    rotatingWall = XWalls[2];
                } else if (Keyboard.GetState().IsKeyDown(Keys.A)) {
                    wallIsRotating = true;
                    rotatingWall = YWalls[0];
                    rotationAxis = Vector3.UnitY;
                } else if (Keyboard.GetState().IsKeyDown(Keys.S)) {
                    wallIsRotating = true;
                    rotatingWall = YWalls[1];
                    rotationAxis = Vector3.UnitY;
                } else if (Keyboard.GetState().IsKeyDown(Keys.D)) {
                    wallIsRotating = true;
                    rotatingWall = YWalls[2];
                    rotationAxis = Vector3.UnitY;
                } else if (Keyboard.GetState().IsKeyDown(Keys.Q)) {
                    wallIsRotating = true;
                    rotatingWall = ZWalls[0];
                    rotationAxis = Vector3.UnitZ;
                } else if (Keyboard.GetState().IsKeyDown(Keys.W)) {
                    wallIsRotating = true;
                    rotatingWall = ZWalls[1];
                    rotationAxis = Vector3.UnitZ;
                } else if (Keyboard.GetState().IsKeyDown(Keys.E)) {
                    wallIsRotating = true;
                    rotatingWall = ZWalls[2];
                    rotationAxis = Vector3.UnitZ;
                }
            }

            //animate rotation of wall
            if (true == wallIsRotating)
                RotateWall(eTime);
            //------------------------

            mouseOld.X = Mouse.GetState().X;
            mouseOld.Y = Mouse.GetState().Y;
            base.Update(gameTime);
        }

        public void RotateWall(float eTime) {
                float a = MathHelper.ToRadians(90);
                currentRotationA += a * eTime / 1000.0f;
                foreach (SmallCube cube in rotatingWall) {
                    //cube.RotateWithAngleTemp(rotationAxis, -a * eTime / 1000.0f);
                    cube.RotateWithAngleTemp(rotationAxis, a * eTime / 1000.0f);
                }
                if (a <= currentRotationA) {
                    foreach (SmallCube cube in rotatingWall)
                        //cube.SetRotationAngle(rotationAxis, -a);
                        cube.SetRotationAngle(rotationAxis, a);
                    wallIsRotating = false;
                    currentRotationA = 0;
                    UpdateWalls();
                }
        }

        private void UpdateWalls() {
            for (int i = 0; i < GameConfig.sideSize; i++) {
                XWalls[i] = new List<SmallCube>();
                YWalls[i] = new List<SmallCube>();
                ZWalls[i] = new List<SmallCube>();
            }
            foreach (SmallCube cube in cubes) {
                XWalls[1 + (int)(cube.Position.X) / 31].Add(cube);
                YWalls[1 + (int)(cube.Position.Y) / 31].Add(cube);
                ZWalls[1 + (int)(cube.Position.Z) / 31].Add(cube);
            }
            visibleWalls[0] = XWalls[GameConfig.sideSize - 1];
            visibleWalls[1] = YWalls[GameConfig.sideSize - 1];
            visibleWalls[2] = ZWalls[GameConfig.sideSize - 1];
            visibleWalls[3] = XWalls[0];
            visibleWalls[4] = YWalls[0];
            visibleWalls[5] = ZWalls[0];
        }

        public void Reset() {
            for (int i = 0; i < cubesCount; i++) {
                cubes[i].ResetRotations();
            }
            rotateAmountX = 0;
            rotateAmountY = 0;
            UpdateWalls();
        }

        private void SetWallColors() {
            int i = GameConfig.sideSize - 1;
            for (int id = 0; id < GameConfig.sideSize * GameConfig.sideSize; id++) {
                ZWalls[i][id].setWallTexture(SmallCube.walls.FRONT, colors[2]);
                YWalls[0][id].setWallTexture(SmallCube.walls.BOTTOM, colors[1]);
                XWalls[0][id].setWallTexture(SmallCube.walls.LEFT, colors[5]);
                YWalls[i][id].setWallTexture(SmallCube.walls.TOP, colors[0]);
                XWalls[i][id].setWallTexture(SmallCube.walls.RIGTH, colors[4]);
                ZWalls[0][id].setWallTexture(SmallCube.walls.BACK, colors[3]);
            }
        }

        public void Shuffle() {
            Random random = new Random(DateTime.Now.Millisecond);
            int countOfRotations = random.Next(1, GameConfig.level);
            for (int i = 0; i < countOfRotations; i++) {
                rotatingWall = XWalls[random.Next(0, GameConfig.sideSize - 1)];
                rotationAxis = Vector3.UnitX;
                currentRotationA = MathHelper.ToRadians(90);
                RotateWall(1f);
                rotatingWall = YWalls[random.Next(0, GameConfig.sideSize - 1)];
                rotationAxis = Vector3.UnitY;
                currentRotationA = MathHelper.ToRadians(90);
                RotateWall(1f);
            }
            RotateCubes(new Vector3(0f, 0f, 0f));
        }

        private void RotateCubes(Vector3 rotation) {
            for (int i = 0; i < cubesCount; i++) {
                cubes[i].Rotate(rotation);
            }
        }

        public override void Draw(GameTime gameTime) {
            for (int i = 0; i < cubesCount; i++) {
                cubes[i].Draw(view, projection);
            }
            base.Draw(gameTime);
        }
    }
}
