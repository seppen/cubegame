﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CubeGame {
    class SmallCube {
        Model model;
        Texture2D baseTexture;

        Texture2D[] wallTextures;
        public enum walls {FRONT = 0, TOP, RIGTH, BOTTOM, BACK, LEFT};

        Vector3 position;
        Vector3 rotation;
        Vector3 scale;
        Matrix r;
        Matrix world;

        public Vector3 Position {
            get { return position; }
        }

        ContentManager content;

        //animation
        Matrix rotationTemp;
        Vector3 positionTemp;

        public SmallCube(Vector3 position, Vector3 scale, ContentManager content, Texture2D baseTexture){
            this.position = position;
            this.positionTemp = position;
            this.scale = scale;
            this.content = content;
            this.world = Matrix.CreateScale(scale) * Matrix.CreateTranslation(position);
            this.model = content.Load<Model>("Content/Models/cube");
            this.baseTexture = baseTexture;
            this.r = Matrix.Identity;
            this.rotationTemp = Matrix.Identity;
            this.wallTextures = new Texture2D[6];
        }

        public void Rotate(Vector3 rotation) {
            this.rotation = rotation;
            this.world = Matrix.CreateScale(scale) * r
                         * Matrix.CreateTranslation(position)
                         * Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);
        }

        public void ResetRotations() {
            Matrix positionM = new Matrix();

            positionM.M11 = position.X;
            positionM.M12 = position.Y;
            positionM.M13 = position.Z;
            positionM.M14 = 0;

            positionM = positionM * Matrix.Invert(r);
            r = Matrix.Identity;
            rotationTemp = Matrix.Identity;

            //popravqme gre6kite v smetkite
            position.X = (Math.Abs(positionM.M11) < 0.001) ? 0 : Math.Sign(positionM.M11) * 31;
            position.Y = (Math.Abs(positionM.M12) < 0.001) ? 0 : Math.Sign(positionM.M12) * 31;
            position.Z = (Math.Abs(positionM.M13) < 0.001) ? 0 : Math.Sign(positionM.M13) * 31;

            positionTemp = position;
            rotation = Vector3.Zero;
        }

        public void Reset() {
            r = Matrix.Identity;
            rotationTemp = Matrix.Identity;
            Rotate(rotation);
        }

        public void setWallTexture(walls wall, Texture2D texture) {
            wallTextures[(int)wall] = texture;
        }

        public Texture2D getWallTexture(walls wall) {
            return wallTextures.ElementAtOrDefault((int)wall);
        }

        public void RotateWithAngleTemp(Vector3 axis, float a) {
            rotationTemp = rotationTemp * Matrix.CreateFromAxisAngle(axis, a);
            world = Matrix.CreateScale(scale) * rotationTemp
                            * Matrix.CreateTranslation(positionTemp)
                            * Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);

            Matrix positionM = new Matrix();

            positionM.M11 = positionTemp.X;
            positionM.M12 = positionTemp.Y;
            positionM.M13 = positionTemp.Z;
            positionM.M14 = 0;
            positionM = positionM * Matrix.CreateFromAxisAngle(axis, a);
            
            positionTemp.X = positionM.M11;
            positionTemp.Y = positionM.M12;
            positionTemp.Z = positionM.M13;
        }

        public void SetRotationAngle(Vector3 axis, float a) {

            Matrix positionM = new Matrix();

            positionM.M11 = position.X;
            positionM.M12 = position.Y;
            positionM.M13 = position.Z;
            positionM.M14 = 0;

            positionM = positionM * Matrix.CreateFromAxisAngle(axis, a);
            r = r * Matrix.CreateFromAxisAngle(axis, a);
            rotationTemp = r;

            //popravqme gre6kite v smetkite
            position.X = (Math.Abs(positionM.M11) < 0.001) ? 0 : Math.Sign(positionM.M11) * 31;
            position.Y = (Math.Abs(positionM.M12) < 0.001) ? 0 : Math.Sign(positionM.M12) * 31;
            position.Z = (Math.Abs(positionM.M13) < 0.001) ? 0 : Math.Sign(positionM.M13) * 31;

            positionTemp = position;
        }

        public void Draw(Matrix view, Matrix projection) {
            int i = 0;
            foreach (ModelMesh mesh in model.Meshes) {
                foreach (ModelMeshPart meshPart in mesh.MeshParts) {
                    BasicEffect effect = (BasicEffect)meshPart.Effect;
                    effect.EnableDefaultLighting();
                    effect.SpecularColor = Color.LightSteelBlue.ToVector3();
                    effect.View = view;
                    effect.Projection = projection;
                    effect.TextureEnabled = true;
                    effect.Texture = wallTextures.ElementAtOrDefault(i);
                    effect.Texture = (null != effect.Texture) ? effect.Texture : baseTexture;
                    effect.World = world;
                    i++;
                }
                mesh.Draw();
            }
        }
    }
}
