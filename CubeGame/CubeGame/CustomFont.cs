﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CubeGame {
    class CustomFont {
        static int charWidth;
        static int charHeight;
        static Texture2D texture;

        public CustomFont(Texture2D font, int width, int height){
            texture = font;
            charWidth = width;
            charHeight = height;
        }

        public int getWidth() {
            return charWidth;
        }

        public int getHeigth() {
            return charHeight;
        }

        public void Draw(SpriteBatch spriteBatch, String text, int spacing, Vector2 start, Color color, float rotation, float scale) {
            for (int i = 0; i < text.Length; i++) {
                spriteBatch.Draw(texture, start, new Rectangle((text[i] - 'A') * charWidth, 0, charWidth, charHeight), color, rotation, Vector2.Zero, scale, SpriteEffects.None, 0f);
                start.X += spacing;
            }
        }
    }
}
