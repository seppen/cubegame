using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CubeGame {
    public class Game1 : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ContentManager content;
        Cube cube;

        enum GameState {Menu, Start, Playing, Success};
        GameState gameState;

        //fonts
        CustomFont textureFont;
        SpriteFont spriteFont;

        //menu strings
        String[] menuItems;
        //menu colors
        Color[] menuItemColors;

        String menuButton;
        Color menuButtonColor;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            content = new ContentManager(Services);

            menuItems = new String[2];
            menuItems[0] = "EASY";
            menuItems[1] = "HARD";

            menuItemColors = new Color[2];
            menuItemColors[0] = Color.White;
            menuItemColors[1] = Color.White;

            menuButton = "MENU";
            menuButtonColor = Color.White;
        }

        protected override void Initialize() {
            this.IsMouseVisible = true;
            this.cube = new Cube(this, content);
            this.gameState = GameState.Menu;
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = content.Load<SpriteFont>("Content/Fonts/Impact");
            textureFont = new CustomFont(content.Load<Texture2D>("Content/Fonts/alphabet"), 57, 43);
        }

        protected override void UnloadContent() {

        }

        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            switch (gameState) {
                case GameState.Menu:
                    UpdateMenu();
                break;
                case GameState.Start:
                    cube.Reset();
                    cube.Shuffle();
                    gameState = GameState.Playing;
                break;
                case GameState.Playing:
                    cube.Update(gameTime);

                    //buton menu
                    if (Mouse.GetState().X <= GraphicsDevice.Viewport.Width
                        && Mouse.GetState().X >= GraphicsDevice.Viewport.Width - 31 * (3 + 1)
                        && Mouse.GetState().Y >= 0
                        && Mouse.GetState().Y <= textureFont.getHeigth() * 0.7f) {
                        menuButtonColor = Color.CornflowerBlue;
                        if (Mouse.GetState().LeftButton == ButtonState.Pressed) {
                            gameState = GameState.Menu;
                        }
                    } else {
                        menuButtonColor = Color.White;
                    }

                    if (true == cube.isSolved()) {
                        gameState = GameState.Success;
                    }
                break;
                case GameState.Success:
                break;
            }
            base.Update(gameTime);
        }

        protected void UpdateMenu() {
            Vector2 menuStart = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
            menuStart.Y = menuStart.Y - menuItems.Length * textureFont.getHeigth();
            for (int i = 0; i < menuItems.Length; i++) {
                if (Mouse.GetState().X <= menuStart.X - textureFont.getWidth() * menuItems[i].Length / 2.0f + textureFont.getWidth() * menuItems[i].Length &&
                    Mouse.GetState().X >= menuStart.X - textureFont.getWidth() * menuItems[i].Length / 2.0f &&
                    Mouse.GetState().Y <= menuStart.Y + textureFont.getHeigth() * (i + 1) &&
                    Mouse.GetState().Y >= menuStart.Y + textureFont.getHeigth() * i) {
                    menuItemColors[i] = Color.CornflowerBlue;
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed) {
                        switch (i){
                            case 0:
                                GameConfig.level = 2;
                                gameState = GameState.Start;
                            break;
                            case 1:
                                GameConfig.level = 5;
                                gameState = GameState.Start;
                            break;
                        }
                    }
                } else {
                    menuItemColors[i] = Color.White;
                }
            }
        }

        protected void DrawMenu() {
            spriteBatch.Begin();
            Vector2 menuStart = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
            menuStart.Y = menuStart.Y - menuItems.Length * textureFont.getHeigth();
            for (int i = 0; i < menuItems.Length; i++) {
                menuStart.X = menuStart.X - (textureFont.getWidth() * menuItems[i].Length) / 2.0f;
                textureFont.Draw(spriteBatch, menuItems[i], textureFont.getWidth(), menuStart, menuItemColors[i], 0f, 1f);
                menuStart.X = menuStart.X + (textureFont.getWidth() * menuItems[i].Length) / 2.0f;
                menuStart.Y += textureFont.getWidth();
            }
            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.Clear(Color.LightSteelBlue);

            switch (gameState) {
                case GameState.Menu:
                    DrawMenu();
                break;
                case GameState.Playing:
                    cube.Draw(gameTime);
                    spriteBatch.Begin();
                    spriteBatch.DrawString(spriteFont, "Rotate:\nX - Z, X, C\nY - A, S, D\nZ - Q, W, E",
                        Vector2.Zero, Color.Snow, 0f, Vector2.Zero, 0.3f, SpriteEffects.None, 0f);
                    textureFont.Draw(spriteBatch, menuButton, 31, new Vector2(GraphicsDevice.Viewport.Width - 31 * menuButton.Length, 0),
                        menuButtonColor, 0f, 0.7f);
                    spriteBatch.End();
                break;
                case GameState.Success:
                    cube.Draw(gameTime);
                    spriteBatch.Begin();
                    Vector2 center = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
                    String text = "CONGRATULATIONS";
                    center.X = center.X - (43 * text.Length) / 2.0f;
                    center.Y = center.Y - textureFont.getHeigth();
                    textureFont.Draw(spriteBatch, text, 43, center, Color.White, 0f, 1f);
                    spriteBatch.End();
                break;
            }
            base.Draw(gameTime);
        }
    }
}
